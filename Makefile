
clean:
	rm -f fuzzyclock
fuzzyclock:
	gcc fuzzyclock.c -o fuzzyclock
install: all
	cp -f fuzzyclock /usr/local/bin
uninstall:
	rm -f /usr/local/bin/fuzzyclock

.PHONY: all install default uninstall
